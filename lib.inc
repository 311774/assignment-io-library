section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall     
    ret  

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    
    .loop:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
		
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
	
	push rdi
    call string_length
	pop rdi
    
    mov rsi, rdi
    mov rdi, 1
    mov rdx, rax
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    mov rax, 1 
    mov rdx, 1 
    push rdi 
    mov rdi, 1 
    mov rsi, rsp 
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov r9, rsp
    mov rax, rdi
    mov r10, 10
    dec rsp
    mov byte [rsp], 0
	
    .loop:
        xor rdx, rdx
        div r10
        or rdx, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jnz .loop
		
    mov rdi, rsp
    call print_string
    mov rsp, r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    test rdi, rdi
    jns .pos
	
    .neg:
        push rdi
		mov rdi, '-'
		call print_char
		pop rdi
		neg rdi
		
    .pos:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor r8, r8
    
    .loop:
        mov cl, byte[rdi + r8]
        mov dl, byte[rsi + r8]
        cmp cl, dl
        jne .error
        inc r8
        test cl, cl
        jnz .loop
        inc rax
        
    .error:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax 
    push rax		
    mov rdi, 0  
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax	
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r8, r8
    
    .skip:
    	push rdi
    	push rsi
    	push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        test rax, rax
        jz .ok
        cmp rax, 0x9
        je .skip
        cmp rax, 0xA
        je .skip
        cmp rax, 0x20
        je .skip

    .read:
        dec rsi
        test rsi, rsi
        jz .error
        mov byte[rdi + r8], al
        inc r8

        push rdi
    	push rsi
    	push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        
        test rax, rax
        jz .ok
        cmp rax, 0x9
        je .ok
        cmp rax, 0xA
        je .ok
        cmp rax, 0x20
        je .ok

        jmp .read

    .ok:
        mov byte[rdi + r8], 0
        mov rax, rdi
        mov rdx, r8
        jmp .end

    .error:
        xor rax, rax

    .end:
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor r8, r8
    mov r10, 10

    .loop:
        mov r8b, byte[rdi + rdx]
        cmp r8b, '9'
        jg .end
        sub r8b, '0'
        jl .end

        imul rax, r10
        add rax, r8
        inc rdx
        jmp .loop

    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    
    cmp byte [rdi], 0x2d
    je .parse
    
    call parse_uint
    ret
    
    .parse:
		inc rdi
		call parse_uint
		cmp rdx, 0
		je .end
		neg rax
		inc rdx
	
    .end:
		ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi 
    call string_length 
    pop rdi
    
    cmp rdx, rax 
    jbe .mini_buffer
	
    .loop:
		push word [rdi]
		pop word [rsi] 
		inc rdi
		inc rsi
		dec rdx
		jbe .end
		jmp .loop

    .mini_buffer:
		xor rax, rax
		ret
		
    .end:
		ret 
